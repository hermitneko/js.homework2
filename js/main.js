let userName;
let userAge;

userName = prompt("What's your name?");
userAge = Number(prompt("How old are you?"));

while(userName === "" ){
    userName = prompt("Try writing your name again");
}

while(userAge === 0 || userAge === "" || isNaN(userAge)){
    userAge = Number(prompt("Try writing your age again"));
}

if(userAge < 18){
    alert("You are not allowed to visit this website");
}
else if(userAge >= 18 && userAge <= 22){
    let confirmAge = confirm("Are you sure you want to continue?");
    if(confirmAge === true){
        alert("Welcome, " + userName + "!");
    }
    else if(confirmAge === false){
        alert("You are not allowed to visit this website");
    }
}
else{
    alert("Welcome, " + userName + "!");
}
